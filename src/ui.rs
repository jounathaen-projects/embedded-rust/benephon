#[allow(unused_imports)]
use core::fmt;
use heapless::{
    consts::{U16, U8},
    String, Vec,
};
use sh1106::{
    mode::graphics::GraphicsMode,
    interface::DisplayInterface,
};
use embedded_graphics::{
    pixelcolor::BinaryColor,
    prelude::*,
    primitives::{Circle, Line, Rectangle},
    style::PrimitiveStyle,
};

use crate::midi::PlayerState;

const STR_LEN: usize = 20;
struct StrBuf {
    buf: [u8; STR_LEN],
}
impl fmt::Write for StrBuf {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        if s.len() < STR_LEN {
            for (i, byte) in s.bytes().enumerate() {
                self.buf[i] = byte;
            }
            return Ok(());
        }
        Err(fmt::Error)
    }
}

pub struct OledUI {
    pub count: u16,
    midi: PlayerState,
}
impl OledUI
{
    pub fn new () -> Self {
        OledUI {
            count: 0,
            midi: PlayerState::Stopped,
        }
    }
    pub fn draw<DI>(&self, disp: &mut GraphicsMode<DI>)
        where DI: DisplayInterface,
              DI::Error: core::fmt::Debug {
        disp.clear();
        let player_status_str = match self.midi {
            PlayerState::Play => "Play",
            PlayerState::Pause => "Pause",
            PlayerState::Stopped => "Stopped",
        };

        Line::new(Point::new(8, 16 + 16), Point::new(8 + 16, 16 + 16))
            .into_styled(PrimitiveStyle::with_stroke(BinaryColor::On, 1))
            .draw(disp)
            .unwrap();

        let mut sound_str: String<U16> = String::from("count: ");
        sound_str
            .push_str(String::<U16>::from(self.count).as_str())
            .unwrap();

        disp.flush().unwrap();
    }
}
