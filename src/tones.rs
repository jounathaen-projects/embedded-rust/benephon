use heapless::{consts::U8, Vec};
use stm32f1xx_hal::{time::Hertz, time::U32Ext};

#[derive(Debug, Copy, Clone)]
pub enum Waveform {
    Sine,
    Square,
    Saw,
}

#[derive(Debug, Copy, Clone)]
pub struct Tone {
    freq: Hertz,
    waveform: Waveform,
    // Idea: Add amplitude
}
impl Tone {
    pub fn sine(f: Hertz) -> Self {
        Tone {
            freq: f,
            waveform: Waveform::Sine,
        }
    }
    pub fn square(f: Hertz) -> Self {
        Tone {
            freq: f,
            waveform: Waveform::Square,
        }
    }
    pub fn saw(f: Hertz) -> Self {
        Tone {
            freq: f,
            waveform: Waveform::Saw,
        }
    }
}

/// Outputs the current amplitude of the resulting tone from tones.
/// This method must be called at a frequency of 8196 Hz and outputs values in the range [0:3600]
pub fn calc_amp(timestep: u16, tones: &Vec<Tone, U8>) -> u16 {
    // static mut
    let mut amp = 0;
    let mut div = 0;
    // induced by the frequency this function is called with. It equals
    // tim3.freq() / steps
    let frequ_corr = 64;
    let max_val: u16 = 160;
    // Our Sine Table reaches to 128. Max_duty is 3600 So here we have a little less than 3600/128
    let amp_corr = 3600 / 128;
    for t in tones.iter() {
        let progress_in_wave = (((timestep as u32 * t.freq.0) / frequ_corr) % 128) as usize;
        amp += match t.waveform {
            Waveform::Sine => SINE_128[progress_in_wave],
            Waveform::Square => {
                if progress_in_wave > 64 {
                    max_val
                } else {
                    0
                }
            }
            Waveform::Saw => progress_in_wave as u16,
        };
        div += 1;
    }
    if div != 0 {
        // durch sqrt(2) teilen bei zwei tönen
        amp * amp_corr / div
    } else {
        0
    }
}

pub fn get_test_tones(count: u16) -> [Option<Tone>; 3] {
    match count % 17 {
        0 => [Some(Tone::sine(440.hz())), None, None],
        1 => [None, Some(Tone::sine(554.hz())), None],
        2 => [Some(Tone::sine(440.hz())), Some(Tone::sine(554.hz())), None],
        3 => [
            Some(Tone::sine(440.hz())),
            Some(Tone::sine(554.hz())),
            Some(Tone::sine(659.hz())),
        ],
        4 => [None, None, None],
        5 => [Some(Tone::square(440.hz())), None, None],
        6 => [None, Some(Tone::square(554.hz())), None],
        7 => [
            Some(Tone::square(440.hz())),
            Some(Tone::square(554.hz())),
            None,
        ],
        8 => [
            Some(Tone::square(440.hz())),
            Some(Tone::square(554.hz())),
            Some(Tone::square(659.hz())),
        ],
        9 => [None, None, None],
        10 => [Some(Tone::saw(440.hz())), None, None],
        11 => [None, Some(Tone::saw(554.hz())), None],
        12 => [Some(Tone::saw(440.hz())), Some(Tone::saw(554.hz())), None],
        13 => [
            Some(Tone::saw(440.hz())),
            Some(Tone::saw(554.hz())),
            Some(Tone::saw(659.hz())),
        ],
        14 => [None, None, None],
        16 => [
            Some(Tone::sine(440.hz())),
            Some(Tone::square(554.hz())),
            Some(Tone::saw(659.hz())),
        ],
        // 5 => [Some(Tone{freq: 329.hz(), waveform: Waveform::Square}), None, None],
        // 6 => [None, Some(Tone::Sine(391.hz())), None],
        // 7 => [Some(Tone::Sine(329.hz())), Some(Tone::new(391.hz())), None],
        // 8 => [Some(Tone::Sine(329.hz())), Some(Tone::new(391.hz())), Some(Tone::new(493.hz()))],
        _ => [None, None, None],
    }
    // [Some(Tone{freq: 329.hz(), waveform: Waveform::Square}), None, None]
}

const SINE_128: [u16; 128] = [
    64, 67, 70, 73, 76, 79, 82, 85, 88, 91, 94, 96, 99, 102, 104, 106, 109, 111, 113, 115, 116,
    118, 120, 121, 122, 123, 124, 125, 126, 126, 127, 127, 127, 127, 127, 126, 126, 125, 124, 123,
    122, 121, 120, 118, 116, 115, 113, 111, 109, 106, 104, 102, 99, 96, 94, 91, 88, 85, 82, 79, 76,
    73, 70, 67, 64, 61, 58, 55, 52, 49, 46, 43, 40, 37, 34, 32, 29, 26, 24, 22, 19, 17, 15, 13, 12,
    10, 8, 7, 6, 5, 4, 3, 2, 2, 1, 1, 1, 1, 1, 2, 2, 3, 4, 5, 6, 7, 8, 10, 12, 13, 15, 17, 19, 22,
    24, 26, 29, 32, 34, 37, 40, 43, 46, 49, 52, 55, 58, 61,
];
