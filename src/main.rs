#![no_std]
#![no_main]
// #![allow(unused_imports)]
#![allow(dead_code)]

// extern crate panic_halt; /* you can put a breakpoint on `rust_begin_unwind` to catch panics */
extern crate panic_semihosting; /* logs messages to the host stderr; requires a debugger */

#[macro_use]
extern crate bitflags;
#[macro_use]
extern crate arrayref;

use cortex_m_semihosting::hprintln;
use stm32f1xx_hal::{
    gpio::*,
    i2c::{BlockingI2c, DutyCycle, Mode},
    pac,
    prelude::*,
    stm32::{I2C2, NVIC},
    time::Hertz,
    timer::{CountDownTimer, Event, Timer},
};

use embedded_hal::digital::v2::OutputPin; // for status_led

use heapless::{consts::U8, Vec};
// use rtfm::cyccnt::{Instant, U32Ext};
use sh1106::mode::GraphicsMode;
use sh1106::{interface::i2c::I2cInterface, Builder};

mod tones;
use tones::*;

mod board;
mod midi;
mod ui;

type I2cDisplay = GraphicsMode<
    I2cInterface<
        BlockingI2c<
            I2C2,
            (
                gpiob::PB10<Alternate<OpenDrain>>,
                gpiob::PB11<Alternate<OpenDrain>>,
            ),
        >,
    >,
>;

#[rtfm::app(device = stm32f1xx_hal::stm32)]
const APP: () = {
    struct Resources {
        main_timer: CountDownTimer<pac::TIM2>,
        sound_timer: CountDownTimer<pac::TIM3>,
        midi_timer: CountDownTimer<pac::TIM1>,
        display: I2cDisplay,
        buttons: board::Buttons,
        letters: board::BenephonLetters,
        onboard_led: stm32f1xx_hal::gpio::gpioc::PC13<Output<PushPull>>,
        pwm: stm32f1xx_hal::pwm::Pwm<stm32f1::stm32f103::TIM4, stm32f1xx_hal::pwm::C4>,
        #[init(None)]
        tones: Option<Vec<Tone, U8>>,
        midi_player: midi::Player,
        oled_ui: ui::OledUI,
    }

    // #[init(schedule = [clear_screen])]
    #[init]
    fn init(_cx: init::Context) -> init::LateResources {
        // get Peripherals
        let p = pac::Peripherals::take().unwrap();
        // let cp = cortex_m::Peripherals::take().unwrap();

        let mut flash = p.FLASH.constrain();
        let mut rcc = p.RCC.constrain();
        let mut afio = p.AFIO.constrain(&mut rcc.apb2);

        let clocks = rcc
            .cfgr
            .use_hse(8.mhz())
            .sysclk(72.mhz())
            .pclk1(32.mhz())
            .adcclk(14.mhz())
            .freeze(&mut flash.acr);
        // let clocks = rcc.cfgr.freeze(&mut flash.acr);

        let mut gpioa = p.GPIOA.split(&mut rcc.apb2);
        let mut gpiob = p.GPIOB.split(&mut rcc.apb2);
        let mut gpioc = p.GPIOC.split(&mut rcc.apb2);

        // LEDS (Use pb3 and pb4 for IO not for JTAG)
        let (_pa15, pb3, pb4) = afio.mapr.disable_jtag(gpioa.pa15, gpiob.pb3, gpiob.pb4);
        let letters = board::BenephonLetters {
            B: board::LedLetter::from(
                gpiob.pb8.into_push_pull_output(&mut gpiob.crh).downgrade(),
                gpioa.pa0.into_pull_up_input(&mut gpioa.crl).downgrade(),
            ),
            E1: board::LedLetter::from(
                gpiob.pb7.into_push_pull_output(&mut gpiob.crl).downgrade(),
                gpioa.pa1.into_pull_up_input(&mut gpioa.crl).downgrade(),
            ),
            N: board::LedLetter::from(
                gpiob.pb6.into_push_pull_output(&mut gpiob.crl).downgrade(),
                gpioa.pa2.into_pull_up_input(&mut gpioa.crl).downgrade(),
            ),
            E2: board::LedLetter::from(
                gpiob.pb5.into_push_pull_output(&mut gpiob.crl).downgrade(),
                gpioa.pa3.into_pull_up_input(&mut gpioa.crl).downgrade(),
            ),
            D: board::LedLetter::from(
                pb4.into_push_pull_output(&mut gpiob.crl).downgrade(),
                gpioa.pa4.into_pull_up_input(&mut gpioa.crl).downgrade(),
            ),
            I: board::LedLetter::from(
                pb3.into_push_pull_output(&mut gpiob.crl).downgrade(),
                gpioa.pa5.into_pull_up_input(&mut gpioa.crl).downgrade(),
            ),
            K: board::LedLetter::from(
                gpiob.pb1.into_push_pull_output(&mut gpiob.crl).downgrade(),
                gpioa.pa6.into_pull_up_input(&mut gpioa.crl).downgrade(),
            ),
            T: board::LedLetter::from(
                gpiob.pb0.into_push_pull_output(&mut gpiob.crl).downgrade(),
                gpioa.pa7.into_pull_up_input(&mut gpioa.crl).downgrade(),
            ),
        };

        let mut buttons = board::Buttons {
            b1: board::Button::from(gpiob.pb12.into_pull_down_input(&mut gpiob.crh).downgrade()),
            b2: board::Button::from(gpiob.pb13.into_pull_down_input(&mut gpiob.crh).downgrade()),
            b3: board::Button::from(gpiob.pb14.into_pull_down_input(&mut gpiob.crh).downgrade()),
            b4: board::Button::from(gpiob.pb15.into_pull_down_input(&mut gpiob.crh).downgrade()),
        };

        for b in buttons.as_mut_array().iter_mut() {
            b.pin.make_interrupt_source(&mut afio);
            b.pin.trigger_on_edge(&p.EXTI, Edge::RISING_FALLING);
            b.pin.enable_interrupt(&p.EXTI);
        }

        unsafe {
            NVIC::unmask(pac::Interrupt::EXTI0);
            NVIC::unmask(pac::Interrupt::EXTI1);
            NVIC::unmask(pac::Interrupt::EXTI2);
            NVIC::unmask(pac::Interrupt::EXTI3);
            NVIC::unmask(pac::Interrupt::EXTI4);
            NVIC::unmask(pac::Interrupt::EXTI9_5);
            NVIC::unmask(pac::Interrupt::EXTI15_10);
        }

        let mut onboard_led = gpioc.pc13.into_push_pull_output(&mut gpioc.crh);
        onboard_led.set_low().unwrap();

        let sound_pin = gpiob.pb9.into_alternate_push_pull(&mut gpiob.crh);
        let pwm =
            Timer::tim4(p.TIM4, &clocks, &mut rcc.apb1).pwm(sound_pin, &mut afio.mapr, 20.khz());

        // pwm.set_period(200.hz());
        // let max = pwm.get_max_duty();
        pwm.enable();
        // pwm.set_duty(max/2);

        // Configure the syst timer to trigger an update every second and enables interrupt
        let mut main_timer = Timer::tim2(p.TIM2, &clocks, &mut rcc.apb1).start_count_down(2.hz());
        main_timer.listen(Event::Update);

        let mut sound_timer =
            Timer::tim3(p.TIM3, &clocks, &mut rcc.apb1).start_count_down(8192.hz());
        sound_timer.listen(Event::Update);

        let mut midi_timer = Timer::tim1(p.TIM1, &clocks, &mut rcc.apb2).start_count_down(100.hz());
        midi_timer.listen(Event::Update);

        // I2C2, alternate open drain
        let scl2 = gpiob.pb10.into_alternate_open_drain(&mut gpiob.crh);
        let sda2 = gpiob.pb11.into_alternate_open_drain(&mut gpiob.crh);

        let iic = BlockingI2c::i2c2(
            p.I2C2,
            (scl2, sda2),
            Mode::Fast {
                frequency: Hertz(400_000),
                duty_cycle: DutyCycle::Ratio2to1,
            },
            clocks,
            &mut rcc.apb1,
            1000,
            10,
            1000,
            1000,
        );

        let mut disp: I2cDisplay = Builder::new().connect_i2c(iic).into();
        disp.init().unwrap();
        // disp.flush().unwrap();

        let midi_player = midi::Player::from(include_bytes!("../assets/midi/Mario.mid"));

        let oled_ui = ui::OledUI::new();

        init::LateResources {
            display: disp,
            main_timer,
            sound_timer,
            midi_timer,
            midi_player,
            onboard_led,
            buttons,
            letters,
            pwm,
            oled_ui,
        }
    }

    #[idle]
    fn idle(_c: idle::Context) -> ! {
        loop {
            cortex_m::asm::wfi();
        }
    }

    /// Does frequent regular "Housekeeping" Tasks: Midi Player update, Button Debounce, and led
    /// lights
    #[task(binds = TIM1_UP, resources = [onboard_led, midi_timer, tones,
        midi_player, letters, buttons], priority = 2)]
    fn status_updates(mut c: status_updates::Context) {
        c.resources.onboard_led.toggle().unwrap();
        // c.resources.onboard_led.set_low().unwrap();
        c.resources.midi_player.update(10); // this is called at 100Hz
        let tones = c.resources.midi_player.get_tones();
        c.resources.tones.lock(|t1| *t1 = tones);

        if let Some(light_arr) = c.resources.midi_player.get_leds() {
            c.resources.letters.activate(&light_arr);
        }

        c.resources.letters.light_and_decay();

        c.resources.buttons.update_debounce_timers();
        // c.resources.onboard_led.set_high().unwrap();
        c.resources.midi_timer.clear_update_interrupt_flag();
    }

    #[task(binds = TIM2, resources = [main_timer, display, buttons, midi_player,
        oled_ui], priority = 1 )]
    fn ui_handling(mut c: ui_handling::Context) {
        let buts_pressed = c.resources.buttons.lock(|b| b.get_pressed());

        c.resources.midi_player.lock(|mp| {
            if buts_pressed.contains(board::ButtonsPressed::B1) {mp.play_pause()};
            if buts_pressed.contains(board::ButtonsPressed::B2) {mp.stop()};
        });

        c.resources.buttons.lock(|b| b.reset());

        // player_status_str = match c.resources.midi_player.status() {
        // midi::PlayerState::Play => "Play",
        // midi::PlayerState::Pause => "Pause",
        // midi::PlayerState::Stopped => "Stopped",
        // };
        // TODO: Update display with that value
        c.resources.oled_ui.count += 1;
        c.resources.oled_ui.draw(c.resources.display);
        c.resources.main_timer.clear_update_interrupt_flag();
    }

    #[task(binds = TIM3, resources = [sound_timer, pwm, tones], priority=3)]
    fn set_sound_output(c: set_sound_output::Context) {
        // c.resources.onboard_led.toggle().unwrap();
        static mut SOUND_COUNTER: u16 = 0;
        *SOUND_COUNTER += 1;
        if *SOUND_COUNTER == 128 * 128 {
            *SOUND_COUNTER = 0;
        }

        if let Some(tones) = c.resources.tones {
            let duty = calc_amp(*SOUND_COUNTER, tones);
            c.resources.pwm.set_duty(duty);
        } else {
            c.resources.pwm.set_duty(0);
        }

        // c.resources.onboard_led.toggle().unwrap();
        c.resources.sound_timer.clear_update_interrupt_flag();
    }

    #[task(binds = EXTI0, resources = [letters], priority = 2)]
    fn exti0(c: exti0::Context) {
        let l = &mut c.resources.letters.B;
        if l.pad.check_interrupt() {
            l.activate();
            l.pad.clear_interrupt_pending_bit();
        }
    }

    #[task(binds = EXTI1, resources = [letters], priority = 2)]
    fn exti1(c: exti1::Context) {
        let l = &mut c.resources.letters.E1;
        if l.pad.check_interrupt() {
            l.activate();
            l.pad.clear_interrupt_pending_bit();
        }
    }

    #[task(binds = EXTI2, resources = [letters], priority = 2)]
    fn exti2(c: exti2::Context) {
        let l = &mut c.resources.letters.N;
        if l.pad.check_interrupt() {
            l.activate();
            l.pad.clear_interrupt_pending_bit();
        }
    }

    #[task(binds = EXTI3, resources = [letters], priority = 2)]
    fn exti3(c: exti3::Context) {
        let l = &mut c.resources.letters.E2;
        if l.pad.check_interrupt() {
            l.activate();
            l.pad.clear_interrupt_pending_bit();
        }
    }

    #[task(binds = EXTI4, resources = [letters], priority = 2)]
    fn exti4(c: exti4::Context) {
        let l = &mut c.resources.letters.D;
        if l.pad.check_interrupt() {
            l.activate();
            l.pad.clear_interrupt_pending_bit();
        }
    }

    #[task(binds = EXTI9_5, resources = [letters], priority = 2)]
    fn exti9_5(c: exti9_5::Context) {
        let l = c.resources.letters;
        let mut letters_arr = [&mut l.I, &mut l.K, &mut l.T];
        for l in letters_arr.iter_mut() {
            if l.pad.check_interrupt() {
                l.activate();
                l.pad.clear_interrupt_pending_bit();
            }
        }
    }

    #[task(binds = EXTI15_10, resources = [buttons], priority = 2)]
    fn exti15_10(c: exti15_10::Context) {
        let buttons = c.resources.buttons;
        for b in buttons.as_mut_array().iter_mut() {
            if b.pin.check_interrupt() {
                b.press_debounced();
                b.pin.clear_interrupt_pending_bit();
            }
        }
    }
};
