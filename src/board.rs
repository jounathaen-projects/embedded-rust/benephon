use stm32f1xx_hal::{
    gpio,
};
use embedded_hal::digital::v2::{InputPin, OutputPin};

const LED_DURATION: u16 = 40;

pub struct LedLetter {
    pub led: gpio::Pxx<gpio::Output<gpio::PushPull>>,
    pub pad: gpio::Pxx<gpio::Input<gpio::PullUp>>,
    pub timer: u16,
}
impl LedLetter {
    pub fn from(
        led: gpio::Pxx<gpio::Output<gpio::PushPull>>,
        pad: gpio::Pxx<gpio::Input<gpio::PullUp>>,
    ) -> Self {
        LedLetter { led, pad, timer: 0 }
    }

    pub fn activate(&mut self) {
        self.timer = LED_DURATION;
    }

    /// Lights the Letter but reduces it's internal timer. If the timer is zero, the Letter is
    /// unlightet
    fn light_and_decay(&mut self) {
        if self.timer > 0 {
            self.timer -= 1;
            self.led.set_high().unwrap();
        } else {
            self.led.set_low().unwrap();
        }
    }
}

#[allow(non_snake_case)]
pub struct BenephonLetters {
    pub B: LedLetter,
    pub E1: LedLetter,
    pub N: LedLetter,
    pub E2: LedLetter,
    pub D: LedLetter,
    pub I: LedLetter,
    pub K: LedLetter,
    pub T: LedLetter,
}
impl BenephonLetters {
    pub fn activate(&mut self, lightmap: &[bool; 8]) {
        lightmap
            .iter()
            .enumerate()
            .filter(|&(_i, l)| *l)
            .for_each(|(i, _l)| self.as_mut_array()[i].activate())
    }

    pub fn light_and_decay(&mut self) {
        self.as_mut_array().iter_mut().for_each(|l| l.light_and_decay());
    }

    pub fn as_array(&self) -> [&LedLetter; 8] {
        [
            &self.B, &self.E1, &self.N, &self.E2, &self.D, &self.I, &self.K, &self.T,
        ]
    }

    pub fn as_mut_array(&mut self) -> [&mut LedLetter; 8] {
        [
            &mut self.B,
            &mut self.E1,
            &mut self.N,
            &mut self.E2,
            &mut self.D,
            &mut self.I,
            &mut self.K,
            &mut self.T,
        ]
    }
}

const DEBOUNCE_TIME: u8 = 25;

pub struct Button {
    pub pin: gpio::Pxx<gpio::Input<gpio::PullDown>>,
    pressed: bool,
    debounce_timer: u8,
}
impl Button {
    pub fn from(pin: gpio::Pxx<gpio::Input<gpio::PullDown>>) -> Self {
        Button {
            pin, pressed: false, debounce_timer: 0,
        }
    }

    pub fn press_debounced(&mut self) {
        if self.pin.is_high().unwrap() && self.debounce_timer == 0 {
            self.pressed = true;
            self.debounce_timer = DEBOUNCE_TIME;
        }
    }

    fn pressed(&self) -> bool {
        self.pressed
    }

    fn update_debounce_timers(&mut self) {
        if self.debounce_timer > 0 {
            self.debounce_timer -= 1;
        }
    }

    fn reset(&mut self) {
        self.pressed = false;
        self.debounce_timer = 0;
    }
}

bitflags! {
    pub struct ButtonsPressed: u8 {
        const B1 = 0b0001;
        const B2 = 0b0010;
        const B3 = 0b0100;
        const B4 = 0b1000;
    }
}

pub struct Buttons {
    pub b1: Button,
    pub b2: Button,
    pub b3: Button,
    pub b4: Button,
}
impl Buttons {
    pub fn update_debounce_timers(&mut self) {
        self.as_mut_array().iter_mut().for_each(|b| b.update_debounce_timers());
    }

    pub fn as_mut_array(&mut self) -> [&mut Button; 4] {
        [
            &mut self.b1,
            &mut self.b2,
            &mut self.b3,
            &mut self.b4,
        ]
    }

    pub fn get_pressed(&self) -> ButtonsPressed {
        let mut but_pressed = ButtonsPressed::empty();
        if self.b1.pressed() {but_pressed |= ButtonsPressed::B1}
        if self.b2.pressed() {but_pressed |= ButtonsPressed::B2}
        if self.b3.pressed() {but_pressed |= ButtonsPressed::B3}
        if self.b4.pressed() {but_pressed |= ButtonsPressed::B4}
        but_pressed
    }

    pub fn reset(&mut self) {
        self.as_mut_array().iter_mut().for_each(|b| b.reset());
    }
}
